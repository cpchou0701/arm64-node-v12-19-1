From arm64v8/python:3.5.3

RUN apt-get install curl -y
RUN apt-get install git -y
RUN apt-get install tar -y

RUN curl -L -o ~/nodejs.tar.gz  https://nodejs.org/download/release/v12.19.1/node-v12.19.1-linux-arm64.tar.gz
RUN gunzip ~/nodejs.tar.gz
RUN tar xvf ~/nodejs.tar 
RUN cd node-v12.19.1-linux-arm64; tar cvf /nodejs.tar .
RUN cd /usr/local;tar xvf /nodejs.tar
RUN ln -s  /usr/local/bin/node /usr/local/bin/nodejs
RUN rm -f ~/nodejs.tar
RUN rm -f /nodejs.tar
RUN rm -rf 
RUN echo "# Nodejs" >> ~/.profile
ENV VERSION=v12.19.1
ENV DISTRO=linux-x64
ENV PORT=9911

RUN npm config set unsafe-perm=true
#RUN cd live-torrent;npm install --unsafe-perm;npm run build


